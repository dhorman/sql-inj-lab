const srs = require('secure-random-string');
const db = require('./db');
const cmd = require('node-cmd');

function generateRandomPassword() {
  return srs({length: 16});
}

function changeLinuxRootPass(pass) {
  cmd.run(`echo 'root:${pass}' | chpasswd`)
}

async function updateRootPassInDb(pass) {
  await db.none(`UPDATE users SET password = '${pass}' WHERE login = 'root'`);
}

async function updateRootPassword() {
  const randomPass = generateRandomPassword();
  changeLinuxRootPass(randomPass);
  await updateRootPassInDb(randomPass);
}

module.exports = {
  updateRootPassword: updateRootPassword
};