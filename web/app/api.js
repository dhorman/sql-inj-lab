const db = require('./db');
const {Router} = require('express');
const router = Router();

const map = {
  async getAll() {
    return await db.manyOrNone("SELECT key, value FROM map");
  },

  async get(key) {
    return await db.oneOrNone(`SELECT value FROM map WHERE key = '${key}'`);
  },

  async put(key, value) {
    return await db.none(`INSERT INTO map (key, value) VALUES ('${key}', '${value}') ON CONFLICT (key) DO UPDATE SET value = '${value}'`);
  },

  async remove(key) {
    return await db.none(`DELETE FROM map WHERE key = '${key}'`);
  }
};

const users = {
  async get(login) {
    return await db.oneOrNone(`SELECT password FROM users WHERE login = '${login}'`);
  }
};


router.get('/map', (req, res) => {
  map.getAll().then(data => {
    res.json(data);
  }).catch(err => {
    res.json(err);
  });
});

router.get('/map/:key', (req, res) => {
  const key = req.params.key;
  map.get(key).then(data => {
    res.json(data);
  }).catch(err => {
    res.json(err);
  });
});

router.put('/map', (req, res) => {
  const data = req.body;
  const key = data.key;
  const value = data.value;
  map.put(key, value).then(() => {
    res.json({status: "ok"});
  }).catch(err => {
    res.json(err);
  });
});

router.delete('/map/:key', (req, res) => {
  const key = req.params.key;
  map.remove(key).then(() => {
    res.json({status: "ok"});
  }).catch(err => {
    res.json(err);
  });
});


/*router.get('/users/:login', (req, res) => {
  const login = req.params.login;
  users.get(login).then(data => {
    res.json(data);
  }).catch(err => {
    res.json(err);
  });
});*/

module.exports = router;