const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const api = require('./api');
const updateRootPassword = require('./core').updateRootPassword;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/api', api);

async function init() {
  await updateRootPassword();
}

app.listen(config.port, () => {
  init()
  .then(() => {
    console.log(`Server started on ${config.port} port...`);
  })
  .catch(err => {
    console.log(`Server not started due to error: ${err}`);
    process.exit(1);
  })
});