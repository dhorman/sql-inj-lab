CREATE TABLE users (
  id       SERIAL PRIMARY KEY,
  login    TEXT UNIQUE,
  password TEXT NOT NULL
);

CREATE TABLE map (
  key   TEXT PRIMARY KEY,
  value TEXT
);

INSERT INTO map (key, value) VALUES ('root', 'ebhe0OXGuzZ1dTdQ');
INSERT INTO users (login, password) VALUES ('root', 'qD7TqnSVsCvE9Lwv');